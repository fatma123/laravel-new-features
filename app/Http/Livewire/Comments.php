<?php

namespace App\Http\Livewire;
use Livewire\WithFileUploads;
use Livewire\WithPagination;
use App\Models\Comment;
use App\Models\User;
use Carbon\Carbon;
use Livewire\Component;

class Comments extends Component
{
    use WithPagination, WithFileUploads;
    public $newComment;
    public $image;


    public function addComment()
    {
        $this->validate(['newComment' => 'required']);
         Comment::create(['body' => $this->newComment, 'user_id' => User::all()->random()->id]);
//        $this->comments->push($createdComment);
//        $this->comments->prepend($createdComment); // make comment in first
        $this->newComment = "";
        session()->flash('message', 'comment successfully updated :D .');

    }

    public function remove($commentId)
    {
        $comment = Comment::find($commentId);
        $comment->delete();
        session()->flash('message', 'comment successfully deleted :( .');
    }


    public function render()
    {
        return view('livewire.comments',[
            'comments' => Comment::paginate(10),
        ]);
    }


}

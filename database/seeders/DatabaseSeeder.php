<?php

namespace Database\Seeders;

use App\Models\Comment;
use App\Models\User;
use Faker\Factory;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
//        Comment::factory(10)->create();
        // \App\Models\User::factory(10)->create();
//        $faker = Factory::create();
//        for ($i = 0; $i < 5; $i++) {
//            Comment::create(
//                [
//                    'body' => $faker->sentence(15),
//                    'user_id' => User::all()->random()->id,
//                ]
//            );
//        }

        $this->call(UserSeeder::class);
        $this->call(CommentSeeder::class);


    }
}
